import 'package:flutter/material.dart';
import 'package:dataviv_task/loginpage.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(
      // DevicePreview(
      //       builder: (context) =>
      MyApp()
      //  )
      );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 690),
      allowFontScaling: false,
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter_ScreenUtil',
        theme: ThemeData(
            // primarySwatch: Colors.cyan,
            primaryColor: Color.fromRGBO(31, 125, 122, 1)),
        home: LoginPage(),
      ),
    );
  }
}
