import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dataviv_task/screen/forgetpassword.dart';
import 'package:dataviv_task/screen/otpverify.dart';
// import './api/api_service.dart';

import './service/networkapirepo.dart';
import './widget/progresHud.dart';
import 'dart:convert';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import './signup.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool hidePassword = true;
  bool isApiCallProcess = false;
  GlobalKey<FormState> _globalFormKey = GlobalKey<FormState>();

  String message = '';
  String token = '';
  final scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPass = TextEditingController();
  TextEditingController user = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String googleimg =
        'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png';
    String fbimg =
        'https://bestforandroid.com/apk/wp-content/uploads/2020/05/facebook-lite-apk-featured-image.png';
    return Scaffold(
      key: scaffoldKey,
      // backgroundColor: Theme.of(context).accentColor,
      body: SingleChildScrollView(
        child:
            // Form(
            //   key: globalFormKey,
            // child:
            Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ClipPath(
              clipper: OvalBottomBorderClipper(),
              child: Container(
                height: 0.4.sh,
                color: Color.fromRGBO(31, 125, 122, 1),
                // child: Center(child: Text("OvalBottomBorderClipper()")),
              ),
            ),
            SizedBox(
              height: 0.01.sh,
            ),
            Form(
              key: _globalFormKey,
              child: Column(
                children: [
                  textField(
                      hint: "Enter Email ID",
                      icon: Icons.mail,
                      textcontroller: userEmail,
                      validator: "Enter Valid Email"),
                  textField(
                      hint: "Password",
                      textcontroller: userPass,
                      validator: "Enter Valid PassWord"),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    ' Sign In ',
                    style: TextStyle(fontSize: 19.sp),
                  ),
                ),
                SizedBox(
                  width: 0.3.sw,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 2.0,
                    clipBehavior: Clip.hardEdge,
                    borderRadius: BorderRadius.circular(50),
                    color: Color.fromRGBO(31, 125, 122, 1),
                    child: InkWell(
                      onTap: () async {
                        if (_globalFormKey.currentState.validate()) {
                          var res = await API_Manager()
                              .login(userEmail.text, userPass.text);
                          print(res);
                          setState(() {
                            message = res['message'];
                            token = res['token'];
                          });
                        }
                        //
                      },

                      // },
                      child: Container(
                        padding: EdgeInsets.all(9.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          // border:
                          //     Border.all(color: Colors.blue, width: 1.4)
                        ),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          size: 22,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                )
                // circlebutton2(userEmail.text, userPass.text),
              ],
            ),
            SizedBox(
              height: 0.02.sh,
            ),
            Container(
                width: 0.5.sw,
                child: Text(
                  message,
                  softWrap: true,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                )),
            SizedBox(
              height: 0.02.sh,
            ),
            Row(
              children: [
                Expanded(
                  child: Divider(
                    thickness: 1,
                    color: Color(0xff818181),
                  ),
                ),
                SizedBox(width: 10),
                Text(
                  'or Login With',
                  style: TextStyle(
                      fontSize: 16.sp,
                      // color: Color(0xff818181),
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Divider(
                    thickness: 1,
                    color: Color(0xff818181),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 0.01.sh,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                circleButton(googleimg),
                circleButton(fbimg),
              ],
            ),
            SizedBox(
              height: 0.01.sh,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'You Don\'t have an account ?',
                  style: TextStyle(fontSize: 16.sp),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => SignUp(),
                    ));
                  },
                  child: Text(
                    'Sign up',
                    style: TextStyle(fontSize: 16.sp),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ForgetPasswordScreen(),
                    ));
                  },
                  child: Text(
                    'Forget Password',
                    style: TextStyle(fontSize: 16.sp),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => OtpVerifyScreen(),
                    ));
                  },
                  child: Text(
                    'OTP verify',
                    style: TextStyle(fontSize: 16.sp),
                  ),
                )
              ],
            )
          ],
        ),
      ),
      // ),
    );
  }

  textField(
      {String hint,
      IconData icon,
      TextEditingController textcontroller,
      String validator}) {
    return Center(
      child: Container(
        width: 0.6.sw,
        decoration: BoxDecoration(),
        child: TextFormField(
          controller: textcontroller,
          cursorColor: Colors.black,
          validator: (value) {
            if (value.isEmpty) {
              return validator;
            }
            return null;
          },
          // keyboardType: TextInputType.,
          decoration: InputDecoration(
            hintStyle: TextStyle(fontSize: 17.sp),
            hintText: hint,
            // prefix: Icon(Icons.mail)
            prefixIcon: Icon(
              icon,
              color: Color.fromRGBO(31, 125, 122, 1),
            ),

            contentPadding: EdgeInsets.all(18),
          ),
        ),
      ),
    );
  }

  circleButton(
    String img,
  ) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        elevation: 2.0,
        clipBehavior: Clip.hardEdge,
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
        child: InkWell(
          onTap: () {
            // API_Manager().login(userEmail, userPass);
          },
          child: Container(
              padding: EdgeInsets.all(9.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                // border: Border.all(color: Colors.blue, width: 1.4)
              ),
              child: Image.network(img, height: 50, width: 50)),
        ),
      ),
    );
  }

  // bool validateAndSave() {
  //   final form = globalFormKey.currentState;
  //   if (form.validate()) {
  //     form.save();
  //     return true;
  //   }
  //   return false;
  // }
}
