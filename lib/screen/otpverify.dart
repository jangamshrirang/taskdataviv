import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../service/networkapirepo.dart';

class OtpVerifyScreen extends StatefulWidget {
  @override
  _OtpVerifyScreenState createState() => _OtpVerifyScreenState();
}

class _OtpVerifyScreenState extends State<OtpVerifyScreen> {
  TextEditingController userEmail = TextEditingController();
  TextEditingController otp = TextEditingController();
  String message = '';
  GlobalKey<FormState> _globalFormKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'OTP Verify',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color.fromRGBO(31, 125, 122, 1),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Form(
            key: _globalFormKey,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 0.1.sh,
                ),
                textField(
                    hint: "Email Address",
                    icon: Icons.email,
                    textcontroller: userEmail,
                    validator: "Enter Email Id"),
                textField(
                    hint: "OTP",
                    icon: Icons.adb_rounded,
                    textcontroller: otp,
                    validator: "Enter OTP"),
                SizedBox(
                  height: 0.01.sh,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 2.0,
                    clipBehavior: Clip.hardEdge,
                    borderRadius: BorderRadius.circular(10),
                    color: Color.fromRGBO(31, 125, 122, 1),
                    child: InkWell(
                      onTap: () async {
                        if (_globalFormKey.currentState.validate()) {
                          var res = await API_Manager().otpVerify(
                              userEmail.text.trim(), otp.text.trim());

                          setState(() {
                            message = res['message'];
                          });
                        }
                      },
                      child: Container(
                          padding: EdgeInsets.all(9.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: Text(
                            ' Verify OTP',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                  ),
                ),
                SizedBox(
                  height: 0.02.sh,
                ),
                Container(
                    width: 0.5.sw,
                    child: Text(
                      message,
                      softWrap: true,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    )),
                SizedBox(
                  height: 0.02.sh,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  textField(
      {String hint,
      IconData icon,
      TextEditingController textcontroller,
      String validator}) {
    return Center(
      child: Container(
        width: 0.6.sw,
        decoration: BoxDecoration(),
        child: TextFormField(
          controller: textcontroller,
          cursorColor: Colors.black,
          validator: (value) {
            if (value.isEmpty) {
              return validator;
            }
            return null;
          },
          // keyboardType: TextInputType.,
          decoration: InputDecoration(
            hintStyle: TextStyle(fontSize: 17.sp),
            hintText: hint,
            // prefix: Icon(Icons.mail)
            prefixIcon: Icon(
              icon,
              color: Color.fromRGBO(31, 125, 122, 1),
            ),
            // suffixIcon: Icon(Icons.search),
            // border: InputBorder.none,
            contentPadding: EdgeInsets.all(18),
          ),
        ),
      ),
    );
  }
}
